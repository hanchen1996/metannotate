#!/bin/bash

START_DIR=$(pwd)

echo "Installing UI version of metAnnotate...\n\nNote that you should have already run base_installation.sh. Also note that this script requires sudo permissions.\n\n"

PATH="${PATH}:${HOME}/.local/bin"

echo "Installing packages and python modules.\n"
# http://mysql-python.sourceforge.net/ - MySQL Python - MySQLdb is a Python DB
# API-2.0-compliant interface
apt-get install python-mysqldb
# https://www.rabbitmq.com/ - RabbitMQ - RabbitMQ is an open source message
# broker software (sometimes called message-oriented middleware) that
# implements the Advanced Message Queuing Protocol (AMQP).
apt-get install rabbitmq-server
apt-get install sqlite3
# http://bottlepy.org/docs/dev/index.html - Bottle is a fast, simple and
# lightweight WSGI micro web-framework for Python. It is distributed as a
# single file module and has no dependencies other than the Python Standard
# Library.
pip install bottle
# https://pypi.python.org/pypi/Beaker? - Beaker is a web session and general
# caching library that includes WSGI middleware for use in web applications.
pip install beaker
# https://pypi.python.org/pypi/ete2 - A Python Environment for (phylogenetic)
# Tree Exploration
pip install ete2
# http://www.paramiko.org/index.html - Paramiko is a Python (2.6+, 3.3+)
# implementation of the SSHv2 protocol [1], providing both client and server
# functionality. While it leverages a Python C extension for low level
# cryptography (PyCrypto), Paramiko itself is a pure Python interface around
# SSH networking concepts.
pip install paramiko
# http://biopython.org/wiki/Main_Page - Biopython is a set of freely available
# tools for biological computation written in Python by an international team
# of developers.
pip install biopython
# https://pypi.python.org/pypi/celery/ - Distributed Task Queue
pip install celery

echo "Downloading and indexing HMMs.\n"
cd precompute/
wget "ftp://ftp.jcvi.org/pub/data/TIGRFAMs/GEN_PROP/PROP_DEF_WITHOUT_DESCRIPTION_FIELD.TABLE"
wget "ftp://ftp.jcvi.org/pub/data/TIGRFAMs/GEN_PROP/PROP_GO_LINK.TABLE"
wget "ftp://ftp.jcvi.org/pub/data/TIGRFAMs/GEN_PROP/PROP_STEP.TABLE"
wget "ftp://ftp.jcvi.org/pub/data/TIGRFAMs/GEN_PROP/STEP_EV_LINK.TABLE"
wget "ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz"
gunzip Pfam-A.hmm.gz
python pfam_splitter.py
wget "ftp://ftp.jcvi.org/pub/data/TIGRFAMs/TIGRFAMs_*_HMM.tar.gz"
tar -xzf TIGRFAMs_* -C ../data/hmms/
python make_hmms_json.py
cd $START_DIR

rm -f precompute/TIGRFAMs_*.tar.gz
rm -f precompute/Pfam-A.hmm

echo -e "Done installing the UI version of metAnnotate.\n\nIMPORTANT: The UI version is not yet ready for running, as you will still need to configure the metagenome directories files. You need to create 2 files:\n\nmetagenome_directories_root.txt\nmetagenome_directories.txt\n\nSee metagenome_directories_sample.txt and metagenome_directories_root_sample.txt for reference. These files need to be placed in the main metannotate direcoty (current directory). metagenome_directories_root.txt contains the root path for all metagenome directories that will be read by the program. metagenome_directories.txt lists all the directories in that root directory that should be read as metagenome directories (in the case that you have other directories in the root directory that shouldn't be interpreted as metagenome directories)."
