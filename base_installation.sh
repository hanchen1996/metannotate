#!/bin/bash

echo "Installing command-line version of metAnnotate...\n"

software=`pwd`/software
START_DIR=$(pwd)
PATH="${PATH}:${HOME}/.local/bin"

mkdir -p downloads
mkdir -p software

# Python Packaging Authority - https://www.pypa.io/en/latest/
echo "Installing pip.\n"
if [ ! `which pip` ] ; then
  cd downloads
  wget "https://bootstrap.pypa.io/get-pip.py"
  python get-pip.py --user --ignore-installed
  cd $START_DIR
fi

# The European Molecular Biology Open Software Suite - http://emboss.open-bio.org/
echo "Installing EMBOSS transeq.\n"
if [ ! `which transeq` ] ; then
  cd downloads
  cp ../bundled_software/EMBOSS* .
#  wget "ftp://emboss.open-bio.org/pub/EMBOSS/old/6.5.0/EMBOSS-6.5.7.tar.gz"
  tar -xzf EMBOSS-6.5.7.tar.gz
  cd EMBOSS*
  ./configure --without-x
  make
  cp -R emboss/ "$software"/
  ln -s "$software"/emboss/transeq ~/.local/bin/transeq
  cd $START_DIR
fi

echo "Installing python packages through pip.\n"
# NumPy is the fundamental package for scientific computing with Python. -
# http://www.numpy.org/
~/.local/bin/pip install --user numpy --ignore-installed
# Celery is an asynchronous task queue/job queue based on distributed message
# passing. It is focused on real-time operation, but supports scheduling
# as well. - http://www.celeryproject.org/
~/.local/bin/pip install --user celery --ignore-installed
# Tools for taxonomic naming and annotation -
# https://pypi.python.org/pypi/taxtastic/0.5.4
~/.local/bin/pip install --user taxtastic --ignore-installed
# The lxml XML toolkit is a Pythonic binding for the C libraries libxml2 and
# libxslt. - http://lxml.de/
~/.local/bin/pip install --user lxml --ignore-installed
# gflags defines a distributed command line system, replacing systems like
# getopt(), optparse and manual argument processing. Rather than an application
# having to define all flags in or near main(), each python module defines
# flags that are useful to it. When one python module imports another, it gains
# access to the other's flags. - https://github.com/gflags/python-gflags
~/.local/bin/pip install --user python-gflags --ignore-installed
# A Python Environment for (phylogenetic) Tree Exploration -
# https://pypi.python.org/pypi/ete2
~/.local/bin/pip install --user ete2 --ignore-installed

# Interactively explore metagenomes and more from a web browser. -
# https://github.com/marbl/Krona
echo "Installing KronaTools.\n"
if [ ! `which ktImportText` ] ; then
  cd included_software/KronaTools-2.5/
  ./install.pl --prefix "${HOME}/.local/"
  cd $START_DIR
fi

# HMMER is used for searching sequence databases for homologs of protein
# sequences, and for making protein sequence alignments. It implements methods
# using probabilistic models called profile hidden Markov models (profile
# HMMs). - http://hmmer.janelia.org/
echo "Installing HMMER & Easel mini-applications.\n"
if [ ! `which hmmsearch` ] | [ ! `which esl-sfetch` ] ; then
  cd downloads
  cp ../bundled_software/hmmer* .
#  wget "ftp://selab.janelia.org/pub/software/hmmer3/3.1b1/hmmer-3.1b1-linux-intel-x86_64.tar.gz"
  tar -xzf hmmer-3.1b2-linux-intel-x86_64.tar.gz
  cd hmmer*
  ./configure
  make
  cp -R . "$software"/hmmer/
  ln -s "$software"/hmmer/binaries/hmmstat ~/.local/bin/hmmstat
  ln -s "$software"/hmmer/binaries/hmmsearch ~/.local/bin/hmmsearch
  ln -s "$software"/hmmer/binaries/hmmalign ~/.local/bin/hmmalign
  ln -s "$software"/hmmer/binaries/esl-reformat ~/.local/bin/esl-reformat
  ln -s "$software"/hmmer/binaries/esl-sfetch ~/.local/bin/esl-sfetch
  cd $START_DIR
fi

# USEARCH is a unique sequence analysis tool with thousands of users
# world-wide. USEARCH offers search and clustering algorithms that are often
# orders of magnitude faster than BLAST. - http://www.drive5.com/usearch/
echo "Installing USEARCH.\n"
if [ ! `which usearch` ] ; then
  ln -s `pwd`"/included_software/usearch" ~/.local/bin/usearch
fi

# FastTree infers approximately-maximum-likelihood phylogenetic trees from
# alignments of nucleotide or protein sequences. -
# http://www.microbesonline.org/fasttree/
echo "Installing FastTreeMP.\n"
if [ ! `which FastTreeMP` ] ; then
#  cd downloads
#  wget "http://www.microbesonline.org/fasttree/FastTreeMP"
  cp bundled_software/FastTreeMP ~/.local/bin/
  chmod a+x ~/.local/bin/FastTreeMP
  #cd ..
fi

# Pplacer places query sequences on a fixed reference phylogenetic tree to
# maximize phylogenetic likelihood or posterior probability according to a
# reference alignment. Pplacer is designed to be fast, to give useful
# information about uncertainty, and to offer advanced visualization and
# downstream analysis.
# guppy is a tool for working with, visualizing, and comparing collections of
# phylogenetic placements, such as those made by pplacer or RAxML’s EPA. “GUPPY”
# is an acronym: Grand Unified Phylogenetic Placement Yanalyzer
# http://matsen.fhcrc.org/pplacer/
echo "Installing pplacer and guppy.\n"
if [ ! `which guppy` ] ; then
  cd downloads
  cp ../bundled_software/pplacer* .
#  wget "http://matsen.fhcrc.org/pplacer/builds/pplacer-v1.1-Linux.tar.gz"
  unzip pplacer-*.zip 
  cd pplacer*
  mv pplacer ~/.local/bin/
  mv guppy ~/.local/bin/
  cd $START_DIR
fi

# Taxononmy information retrieved from NCBI - https://www.ncbi.nlm.nih.gov/
# The National Center for Biotechnology Information advances science and health
# by providing access to biomedical and genomic information.
echo "Downloading and indexing taxonomy info.\n"
if [ ! -e data/taxonomy.pickle ] ; then
  cd precompute
  wget "ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz"
  tar -zxf taxdump.tar.gz
  grep 'scientific name' names.dmp > trimmed.names.dmp
# Creates a python data structure
  python make_taxonomy_pickle.py
  cd $START_DIR
fi

# GI number (sometimes written in lower case, "gi") is simply a series of
# digits that are assigned consecutively to each sequence record processed by
# NCBI. The GI number bears no resemblance to the Accession number of the
# sequence record.
# Taxids are assigned sequentially as taxa are added to the taxonomy database,
# and are never reused for a different taxon once they have been assigned. When
# two taxa are determined to be synonymous they are merged, and the taxid that
# disappears becomes a ‘secondary’ taxid for the one that remains. Taxa are
# occasionally removed from the taxonomy database (particularly internal nodes,
# during a taxonomic revision) – these taxids are deleted, and are not reused.
# Lists of merged and deleted taxids are included in the taxonomy dump files on
# the ftp site.
echo "Downloading and indexing gi number to taxid mappings.\n"
if [ ! -e data/gi_taxid_prot.dmp ] ; then
  cd precompute
  wget "ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/gi_taxid_prot.dmp.gz"
  gunzip gi_taxid_prot.dmp.gz
  mv gi_taxid_prot.dmp ../data/
  cd $START_DIR
fi

rm -rf downloads
rm -f precompute/gc.prt
rm -f precompute/readme.txt
rm -f precompute/taxdump.tar.gz

echo "$HOME/.local/bin/" > path.txt

echo -e "Prerequisites have been installed and the command line version of metAnnotate has been set up.\n\nIMPORTANT: metAnnotate is still not fully ready to be run. You need to download the refseq database and place it in the data directory (metannotate/data/) as \"Refseq.fa\". You also need to place the ssi index of this file in the same directory, as \"Refseq.fa.ssi\". To build Refseq.fa, desired files can be downloaded from \"ftp://ftp.ncbi.nlm.nih.gov/refseq/release/\" and concatenated. Alternatively, this fasta file can be generated from local NCBI blastdb files. To create the ssi index, simply run \"esl-sfetch —index Refseq.fa\" when in the data directory.\n\nTo install the web UI version of metAnnotate, please run the full_installation.sh script with sudo permissions."
